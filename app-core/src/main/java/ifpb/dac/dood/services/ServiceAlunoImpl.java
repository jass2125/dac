package ifpb.dac.dood.services;

import ifpb.dac.dood.model.Aluno;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import ifpb.dac.dood.ServiceAluno;
import javax.persistence.Persistence;

/**
 *
 * @author Ricardo Job
 */
@Stateless
@Remote(ServiceAluno.class)
public class ServiceAlunoImpl implements ServiceAluno {

    private EntityManager em;

    public ServiceAlunoImpl() {
        em = Persistence.createEntityManagerFactory("dood").createEntityManager();
    }

    @Override
    public void salvar(Aluno pessoa) {
        em.persist(pessoa);
    }

}
