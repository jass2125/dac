/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifpb.dac.dood.controller;

import ifpb.dac.dood.ConviteService;
import ifpb.dac.dood.model.Convite;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;



/**
 *
 * @author Anderson Souza
 */
@Named
@RequestScoped
public class ConviteControlador {

    private boolean renderPerfil, renderEditar, renderCancelar;

    @EJB
    private ConviteService conviteService;

    private Convite convite;

    public ConviteControlador() {
        this.convite = new Convite();
        this.renderPerfil = true;
        this.renderEditar = false;
        this.renderCancelar = false;
    }

    public Convite getConvite() {
        return convite;
    }

    public boolean isRenderPerfil() {
        return renderPerfil;
    }

    public void setRenderPerfil(boolean renderPerfil) {
        this.renderPerfil = renderPerfil;
    }

    public boolean isRenderEditar() {
        return renderEditar;
    }

    public void setRenderEditar(boolean renderEditar) {
        this.renderEditar = renderEditar;
    }

    public boolean isRenderCancelar() {
        return renderCancelar;
    }

    public void setRenderCancelar(boolean renderCancelar) {
        this.renderCancelar = renderCancelar;
    }

    public String submeterConvite() {
        this.conviteService.addConvite(this.convite);
        return "confirmar-cadastro.xhtml";
    }

    public void showPerfil() {
        renderPerfil = true;
        renderEditar = false;
        renderCancelar = false;
    }

    public void showEditar() {
        renderPerfil = false;
        renderEditar = true;
        renderCancelar = false;
    }

}
