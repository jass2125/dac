/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifpb.dac.dood.services;

import ifpb.dac.dood.ConviteService;
import ifpb.dac.dood.model.Convite;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author Anderson Souza
 */
@Stateless
@Remote(ConviteService.class)
public class ConviteServiceImpl implements ConviteService {

    private EntityManager em;

    public ConviteServiceImpl() {
        em = Persistence.createEntityManagerFactory("dood").createEntityManager();
    }

    @Override
    public boolean addConvite(Convite convite) {
        em.getTransaction().begin();
        em.persist(convite);
        em.getTransaction().commit();
        return true;
    }

}
