/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifpb.dac.dood.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 *
 * @author Anderson Souza
 */
@Entity
public class Professor extends Usuario implements Serializable {

    private String formacaoAcademica;
    @OneToMany(targetEntity = Grupo.class, mappedBy = "criador")
    private List<Grupo> listaDeGruposCriados;

    public Professor() {
    }

    public String getFormacaoAcademica() {
        return formacaoAcademica;
    }

    public void setFormacaoAcademica(String formacaoAcademica) {
        this.formacaoAcademica = formacaoAcademica;
    }

    public List<Grupo> getListaDeGruposCriados() {
        return listaDeGruposCriados;
    }

    public void setListaDeGruposCriados(List<Grupo> listaDeGruposCriados) {
        this.listaDeGruposCriados = listaDeGruposCriados;
    }

    
}
