/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifpb.dac.dood.model;

/**
 *
 * @author Anderson Souza
 */
public enum SexoEnum {
    MASC("Masculino"), FEM("Feminino");
    
    private String sexo;

    private SexoEnum(String sexo) {
        this.sexo = sexo;
    }

    public String getSexo() {
        return sexo;
    }

}
