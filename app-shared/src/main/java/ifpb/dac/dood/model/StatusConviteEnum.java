/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifpb.dac.dood.model;

/**
 *
 * @author Luciana
 */
public enum StatusConviteEnum {

    ATIVO("Ativo"), DESATIVO("Inativo");

    private String status;

    private StatusConviteEnum(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

}
