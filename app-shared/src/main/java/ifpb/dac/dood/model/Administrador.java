/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifpb.dac.dood.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 *
 * @author Anderson Souza
 */
@Entity
public class Administrador extends Usuario implements Serializable {

    @OneToMany(targetEntity = Grupo.class, mappedBy = "admcriador")
    private List<Grupo> listaDeGruposCriados;
    
   
    public Administrador() {
        super();
    }

    public List<Grupo> getListaDeGruposCriados() {
        return listaDeGruposCriados;
    }

    public void setListaDeGruposCriados(List<Grupo> listaDeGruposCriados) {
        this.listaDeGruposCriados = listaDeGruposCriados;
    }

   

}
