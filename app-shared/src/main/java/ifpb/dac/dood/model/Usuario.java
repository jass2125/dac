/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifpb.dac.dood.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Anderson Souza
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@NamedQueries({
    @NamedQuery(name = "Usuario.login", query = "SELECT u FROM Usuario u WHERE u.email = :email AND u.senha = :senha")
})
public class Usuario implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idUsuario;
    private String nome;
    private String email;
    private String senha;

    @Basic(fetch = FetchType.LAZY)
    @Lob
    private byte[] foto;
    @Temporal(TemporalType.DATE)
    private Date dtNasc;
    @Column(unique = true)
    private String matricula;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Telefone> telefones;

    @Enumerated(EnumType.STRING)
    private SexoEnum sexo;

    @Enumerated(EnumType.STRING)
    private UsuarioStatusEnum status;

    @ManyToMany
    private List<Usuario> seguidores;
    @ManyToMany
    private List<Usuario> seguindo;

    @OneToMany
    private List<Arquivo> arquivos;

    public Usuario() {
        this.telefones = new ArrayList<Telefone>();
    }

    public Usuario(String nome, String email, String senha, byte[] foto, Date dtNasc, String matricula, List<Telefone> telefones, SexoEnum sexo, UsuarioStatusEnum status, List<Usuario> seguidores, List<Usuario> seguindo, List<Arquivo> arquivos) {
        this.nome = nome;
        this.email = email;
        this.senha = senha;
        this.foto = foto;
        this.dtNasc = dtNasc;
        this.matricula = matricula;
        this.telefones = telefones;
        this.sexo = sexo;
        this.status = status;
        this.seguidores = seguidores;
        this.seguindo = seguindo;
        this.arquivos = arquivos;
    }

    
    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    public Date getDtNasc() {
        return dtNasc;
    }

    public void setDtNasc(Date dtNasc) {
        this.dtNasc = dtNasc;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public List<Telefone> getTelefones() {
        return telefones;
    }

    public void setTelefones(List<Telefone> telefones) {
        this.telefones = telefones;
    }

    public SexoEnum getSexo() {
        return sexo;
    }

    public void setSexo(SexoEnum sexo) {
        this.sexo = sexo;
    }

    public UsuarioStatusEnum getStatus() {
        return status;
    }

    public void setStatus(UsuarioStatusEnum status) {
        this.status = status;
    }

    public List<Usuario> getSeguidores() {
        return seguidores;
    }

    public void setSeguidores(List<Usuario> seguidores) {
        this.seguidores = seguidores;
    }

    public List<Usuario> getSeguindo() {
        return seguindo;
    }

    public void setSeguindo(List<Usuario> seguindo) {
        this.seguindo = seguindo;
    }

    public List<Arquivo> getArquivos() {
        return arquivos;
    }

    public void setArquivos(List<Arquivo> arquivos) {
        this.arquivos = arquivos;
    }

}
