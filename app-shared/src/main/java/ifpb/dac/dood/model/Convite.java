/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifpb.dac.dood.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 *
 * @author Anderson Souza
 */
@Entity
public class Convite implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idConvite;
    private String email;
    @Enumerated
    private StatusConviteEnum status;

    @OneToOne
    private Usuario usuario;

    @ManyToOne
    private Administrador admin;

    public Convite() {
    }

    public Long getIdConvite() {
        return idConvite;
    }

    public void setIdConvite(Long idConvite) {
        this.idConvite = idConvite;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public StatusConviteEnum getStatus() {
        return status;
    }

    public void setStatus(StatusConviteEnum status) {
        this.status = status;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Administrador getAdmin() {
        return admin;
    }

    public void setAdmin(Administrador admin) {
        this.admin = admin;
    }

}
